import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

  email1: string;
  password: string;
  name: string;
  error='';
  result:boolean;
  wrong = '';


signUp(){
  this.result = /^[a-z0-9]+$/i.test(this.password);
  if(this.result) {
    this.wrong = "you must have special character";
    return
  }
  console.log("sign up clicked " + this.email + ' ' + this.name + ' ' + this.password);
  this.authService.signup(this.email1,this.password)
    .then(value => {
      this.authService.updateProfile(value.user,this.name);
      }).then(value => {
        this.router.navigate(['welcome'])
      })
      .catch(err => {
        this.error = err;
        console.log(err);
        })
}

}
